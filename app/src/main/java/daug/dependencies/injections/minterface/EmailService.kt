package daug.dependencies.injections.minterface

import android.util.Log

/**
 * Created by Hospice on 20/03/2018.
 */
class EmailService : MessageService {
    override fun sendMessage(subject: String, message: String) {
        Log.e(EmailService::class.java.name, "Email: $subject, $message")
    }
}
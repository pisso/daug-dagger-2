package daug.dependencies.injections.minterface

import android.util.Log

/**
 * Created by Hospice on 20/03/2018.
 */
class SmsService : MessageService {
    override fun sendMessage(subject: String, message: String) {
        Log.e(SmsService::class.java.name, "Sms: $subject, $message")
    }
}

package daug.dependencies.injections.data

import daug.dependencies.injections.minterface.MessageService
import javax.inject.Inject

/**
 * Created by Hospice on 20/03/2018.
 */
/*
class Person(private val messageService: MessageService) {

    fun greetFriend(subject : String, message: String) {
        messageService.sendMessage(subject, message)
    }
}*/

//class Person {
//    fun greetFriend(subject : String, message: String) {
//        val mService  = Factory().messageServiceDependency
//        mService.sendMessage(subject,message)
//    }
//}


class Person @Inject constructor(private val messageService: MessageService) : PersonInterface {
    override fun greetFriend(subject: String, message: String) {
        messageService.sendMessage(subject, message)
    }
}




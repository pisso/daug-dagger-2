package daug.dependencies.injections.data

/**
 * Created by Hospice on 22/03/2018.
 */
interface PersonInterface {

    fun greetFriend(subject : String, message: String)

}
package daug.dependencies.injections.factoryWay

import daug.dependencies.injections.minterface.EmailService
import daug.dependencies.injections.minterface.MessageService

/**
 * Created by Hospice on 23/03/2018.
 */

class Factory {
    val messageServiceDependency: MessageService
        get() = EmailService()
}

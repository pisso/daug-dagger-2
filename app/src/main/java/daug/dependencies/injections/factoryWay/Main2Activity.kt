package daug.dependencies.injections.factoryWay

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import daug.dependencies.injections.R

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val mService  = Factory().messageServiceDependency
         mService.sendMessage("subject","message")
    }


}

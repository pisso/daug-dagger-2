package daug.dependencies.injections.dinewway.ui

import android.os.Bundle
import android.widget.Toast
import daug.dependencies.injections.R
import daug.dependencies.injections.data.Person
import daug.dependencies.injections.dinewway.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainActivityContrat.View {

    @Inject
    lateinit var mPresenter: MainActivityContrat.Presenter<MainActivityContrat.View>



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mPresenter.onAttach(this)

        sendMessageButton.setOnClickListener {
            mPresenter.sendMessage(subject.text.toString(), message.text.toString())
        }

    }


    override fun setSubjectError() {
        subject.error = getString(R.string.mandatory_field)
    }

    override fun setMessageError() {
        message.error = getString(R.string.mandatory_field)
    }

    override fun showToast() {
        Toast.makeText(this, getString(R.string.message_sent), Toast.LENGTH_LONG).show()
    }


}

package daug.dependencies.injections.dinewway.ui

import daug.dependencies.injections.dinewway.ui.base.MvpPresenter
import daug.dependencies.injections.dinewway.ui.base.MvpView

/**
 * Created by Hospice on 21/03/2018.
 */

interface MainActivityContrat {

    interface View  : MvpView{
        fun setSubjectError()
        fun setMessageError()
        fun showToast()

    }

    interface Presenter<V : View > : MvpPresenter<V> {
        fun sendMessage(subject: String, message: String)
    }

}

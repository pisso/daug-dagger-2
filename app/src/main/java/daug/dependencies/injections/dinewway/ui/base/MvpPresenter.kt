package daug.dependencies.injections.dinewway.ui.base

/**
 * Created by Hospice on 22/03/2018.
 */

interface MvpPresenter<in V : MvpView> {

    fun onAttach(mvpView: V)

    fun onDetach()

}
package daug.dependencies.injections.dinewway.module

import android.content.Context
import dagger.Module
import dagger.Provides
import daug.dependencies.injections.data.Person
import daug.dependencies.injections.data.PersonInterface
import daug.dependencies.injections.dinewway.MyApplication
import daug.dependencies.injections.minterface.EmailService
import daug.dependencies.injections.minterface.MessageService
import javax.inject.Singleton

/**
 * Created by Hospice on 20/03/2018.
 */

@Module
class ApplicationModule {


    @Provides
    @Singleton
    fun provideContext(mApplication : MyApplication): Context {
        return mApplication
    }


    @Provides
    @Singleton
    fun provideMessageService(): MessageService {
        return EmailService()
    }


    @Provides
    @Singleton
    fun providePerson(person: Person): PersonInterface {
        return person
    }



}
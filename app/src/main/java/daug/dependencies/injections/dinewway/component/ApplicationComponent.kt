package daug.dependencies.injections.dinewway.component

import android.app.Application

import javax.inject.Singleton

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import daug.dependencies.injections.dinewway.MyApplication
import daug.dependencies.injections.dinewway.activitybuilder.ActivityBuilder
import daug.dependencies.injections.dinewway.module.ApplicationModule

/**
 * Created by Hospice on 20/03/2018.
 */

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, ApplicationModule::class, ActivityBuilder::class])
interface ApplicationComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent

    }

    fun inject(app: MyApplication)

}

package daug.dependencies.injections.dinewway.ui.base

import android.support.annotation.StringRes

/**
 * Created by Hospice on 22/03/2018.
 */

interface MvpView {
    fun showLoading()

    fun hideLoading()
    fun onError(@StringRes resId: Int)

    fun onError(message: String)

    fun showMessage(message: String)

    fun showMessage(@StringRes resId: Int)
}

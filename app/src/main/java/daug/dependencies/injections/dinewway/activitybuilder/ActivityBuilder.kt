package daug.dependencies.injections.dinewway.activitybuilder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import daug.dependencies.injections.dinewway.ui.MainActivity
import daug.dependencies.injections.dinewway.module.MainActivityModule

/**
 * Created by Hospice on 20/03/2018.
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
     abstract fun contributeMainActivity(): MainActivity

}

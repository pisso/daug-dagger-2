package daug.dependencies.injections.dinewway.module

import dagger.Binds
import dagger.Module
import daug.dependencies.injections.dinewway.ui.MainActivityContrat
import daug.dependencies.injections.dinewway.ui.MainPresenter


/**
 * Created by Hospice on 20/03/2018.
 */

@Module
abstract class MainActivityModule {

    @Binds
    abstract fun provideMainPresenter(presenter: MainPresenter<MainActivityContrat.View>): MainActivityContrat.Presenter<MainActivityContrat.View>

}

package daug.dependencies.injections.dinewway

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerApplication
import daug.dependencies.injections.dinewway.component.DaggerApplicationComponent
import javax.inject.Inject

/**
 * Created by Hospice on 20/03/2018.
 */

class MyApplication : DaggerApplication()  {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerApplicationComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }

}


package daug.dependencies.injections.dinewway.ui.base


/**
 * Created by Hospice on 22/03/2018.
 */

open class BasePresenter<V : MvpView> : MvpPresenter<V> {
    lateinit var mMvpView: V

    override fun onAttach(mvpView: V) {
        mMvpView = mvpView
    }

    override fun onDetach() {

    }
}

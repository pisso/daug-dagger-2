package daug.dependencies.injections.dinewway.ui.base

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity

open class BaseActivity : DaggerAppCompatActivity() , MvpView{


    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun onError(resId: Int) {
     }

    override fun onError(message: String) {
    }

    override fun showMessage(message: String) {
     }

    override fun showMessage(resId: Int) {
    }


}

package daug.dependencies.injections.dinewway.ui

import android.text.TextUtils
import daug.dependencies.injections.data.PersonInterface
import daug.dependencies.injections.dinewway.ui.base.BasePresenter
import javax.inject.Inject

/**
 * Created by Hospice on 22/03/2018.
 */


class  MainPresenter<V : MainActivityContrat.View> @Inject constructor(var person : PersonInterface)
    : BasePresenter<V>(), MainActivityContrat.Presenter<V> {

    override fun sendMessage(subject: String, message: String) {

        when{

            TextUtils.isEmpty(subject) -> mMvpView.setSubjectError()
            TextUtils.isEmpty(message) -> mMvpView.setMessageError()

            else ->{
                person.greetFriend(subject,message)
                mMvpView.showToast()

            }
        }
    }


}
